<?php

function gap($g, $m, $n) {
    for ($i = $m; $i < $n; $i++) {
      if (isPrime($i)) {
        $next = findNextPrime($i);
  
        if ($next - $i === $g) 
          return array($i, $next);
      }
    }
  
    return null;
  }
  
  function findNextPrime($prime) {
    while ($prime++) {
      if (isPrime($prime))
        return $prime;
    }
  }
  
  function isPrime($n) {
    if ($n <= 1)
      return 0;
  
    for ($i = 2; $i <= sqrt($n); $i++) {
      if ($n % $i === 0)
        return 0;
    }
  
    return 1;
  }