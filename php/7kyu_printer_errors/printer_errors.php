<?php

function printerError($s) {
    $errs = 0;
    $total = strlen($s);
    
    for ($i = 0; $i < strlen($s); $i++){
      if ($s[$i] > 'm')
        $errs++;
    }
    
    return "{$errs}/{$total}";
  }