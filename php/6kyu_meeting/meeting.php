<?php

function meeting($s) {
    $attendees = explode(';', strtoupper($s));
    $attendeesString = null;
    
    $attendees = getSortedAttendeesArray($attendees);

    foreach($attendees as $attendee)
        $attendeesString .= "({$attendee[0]}, {$attendee[1]})";

    return $attendeesString;
}

function getSortedAttendeesArray(array $attendees) : array {
    $result = array();

    foreach ($attendees as $attendee) {
        $attendee = explode(':', $attendee);
        $attendee = array_reverse($attendee);
    
        array_push($result, $attendee);
    }

    sort($result);
    
    return $result;
}