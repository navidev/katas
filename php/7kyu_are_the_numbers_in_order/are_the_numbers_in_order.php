<?php

function in_asc_order($arr) {
    if (count($arr) == 1) {
      return true;
    }
      
    foreach ($arr as $key => $number) { 
      if ($arr[$key] < $arr[$key -1]) {
        return false;
      }
    }
    
    return true;
  }