<?php

function invite_more_women(array $a): bool {
  $women = 0;
  $men = 0;
  $goers = count($a);
  
  foreach($a as $goer) {
    ($goer === -1) ? $women++ : $men++;
  }
  
  if ($women < $men)
    return true;
  
  return false;
}