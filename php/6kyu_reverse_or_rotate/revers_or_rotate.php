<?php

function revRot($s, $sz) {
    if ($sz <= 0 || $sz > strlen($s))
      return '';
    
    $res = ''; 
    $s = str_split($s, $sz);
    
    if (strlen(end($s)) < $sz)
      array_pop($s);
    
    foreach ($s as $integer) {
      $digits = str_split($integer);
      
      if ((array_sum($digits) ** 2) % 2 === 0) {
        $integer = strrev($integer);
        $res .= $integer;
      } else {
        array_push($digits, array_shift($digits));
        $res .= implode($digits);
      }
    }
        
    return $res;
  }