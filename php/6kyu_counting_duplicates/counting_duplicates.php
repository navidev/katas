<?php

function duplicateCount($text) {
    if (empty($text)) 
      return 0;
    
    $text = strtolower($text);
    $arr = str_split($text);
    $duplicates = array();
    
    foreach ($arr as $char){ 
      if (substr_count($text, $char) > 1 && !in_array($char, $duplicates))
        array_push($duplicates, $char);    
    }
    
    return count($duplicates);
  }