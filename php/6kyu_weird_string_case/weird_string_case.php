<?php

function toWeirdCase($string) {
    $words = explode(' ', strtolower($string));
    $result = array();

    foreach ($words as $key => $word) {
        $word = str_split($word);
        
        foreach ($word as $key => &$char) {
            if (strcmp($char, ' ') !== 0 && $key % 2 === 0)
                $char = strtoupper($char);        
        }

        array_push($result, $word);
    }

    return implode(' ', array_map('implode', $result));
}