<?php

class SnakesLadders { 
    private $pTurn;
    private $players;
    private $gameover;
    private $ladders;
    private $snakes;
    
    function __construct() {
      $this->pTurn = 1;
      $this->gameover = false;
      $this->ladders = array(
        2 => 38, 7 => 14, 8 => 31, 15 => 26, 21 => 42,
        28 => 84, 36 => 44, 51 => 67, 71 => 91, 78 => 98, 
        87 => 94,
      );
      $this->snakes = array(
        16 => 6, 46 => 25, 49 => 11, 62 => 19, 64 => 60,
        74 => 53, 89 => 68, 92 => 88, 95 => 75, 99 => 80,
      );
      $this->players = array(
        1 => array('pos' => 0), 
        2 => array('pos' => 0));
    }
  
    public function play($die1, $die2) {
        if ($this->gameover) {
            return 'Game over!';
        }
      
        $p = $this->pTurn;
        $replay = ($die1 === $die2) ? true : false;
        $dicesSum = $die1 + $die2;
        $pos = $this->players[$p]['pos'] += $dicesSum;
        
        if ($pos === 100) {
            $this->gameover = true;
            return "Player {$p} Wins!";
        } elseif ($pos > 100) {
            $pos = 100 - ($pos - 100);
            $this->players[$p]['pos'] = $pos;
        }

        if (array_key_exists($pos, $this->snakes)) {
            $this->players[$p]['pos'] = $this->snakes[$pos];
        } elseif (array_key_exists($pos, $this->ladders)) {
            $this->players[$p]['pos'] = $this->ladders[$pos];
        }

        if (!$replay) {
            $this->pTurn = ($this->pTurn === 1) ? 2 : 1;
        }
        
        return "Player {$p} is on square {$this->players[$p]['pos']}";
    }
  }