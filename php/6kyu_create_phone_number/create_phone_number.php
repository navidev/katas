<?php

function createPhoneNumber($numbersArray) {
  $str = implode('', $numbersArray);
  $res = sprintf("(%s) %s-%s", 
                 substr($str, 0, 3), 
                 substr($str, 3, 3), 
                 substr($str, 6));
  
  return $res;
}
