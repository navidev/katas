<?php

function rgb($r,$g,$b) {
    $rgb = func_get_args();
    
    foreach($rgb as &$c) {
      if ($c < 0) 
        $c = 0;
      else if ($c > 255) 
        $c = 255;
    }
    
    return sprintf("%02X%02X%02X", $rgb[0], $rgb[1], $rgb[2]);
  }