<?php

function solve($arr) {
    foreach ($arr as $key => $digit) {
      if (array_count_values($arr)[$digit] > 1)
        unset($arr[$key]);
    }
  
    return array_values($arr);
  }