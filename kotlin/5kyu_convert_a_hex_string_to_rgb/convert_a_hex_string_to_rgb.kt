
fun hexStringToRGB(hexString: String): RGB {
    val args: MutableList<Int> = ArrayList()
    
    hexString.removePrefix("#").chunked(2).forEach {
        args.add(it.toInt(radix = 16))
    }
    
    return RGB(args[0], args[1], args[2])
}