fun growingPlant(upSpeed: Int, downSpeed: Int, desiredHeight: Int): Int {
    if (upSpeed >= desiredHeight) return 1
    
    var size: Int = 0
    
    for (i in 0..desiredHeight) {
        when (i) {
            0 -> size += upSpeed  
            else -> size += (upSpeed - downSpeed) 
    }
        
    if (size >= desiredHeight)
        return i + 1
    }
 
    return 0
}